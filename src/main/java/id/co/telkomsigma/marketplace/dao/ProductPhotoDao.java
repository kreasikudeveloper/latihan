package id.co.telkomsigma.marketplace.dao;

import org.springframework.data.repository.PagingAndSortingRepository;

import id.co.telkomsigma.marketplace.entity.Product;
import id.co.telkomsigma.marketplace.entity.ProductPhoto;

public interface ProductPhotoDao extends PagingAndSortingRepository<ProductPhoto, String> {
    public Iterable<ProductPhoto> findByProduct(Product product);
}
