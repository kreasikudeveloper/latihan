package id.co.telkomsigma.marketplace.dao;

import org.springframework.data.repository.PagingAndSortingRepository;

import id.co.telkomsigma.marketplace.entity.Product;

public interface ProductDao extends PagingAndSortingRepository<Product, String> {

}
