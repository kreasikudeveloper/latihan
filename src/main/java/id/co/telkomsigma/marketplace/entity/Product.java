package id.co.telkomsigma.marketplace.entity;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.NotEmpty;

import lombok.Data;

@Entity @Data
public class Product {
	
	@Id @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @NotNull @NotEmpty @Size(max = 100)
    private String code;

    @NotNull @NotEmpty
    private String name;
    
    @NotNull @Min(1)
    private BigDecimal weight;

    @NotNull @Min(1000)
    private BigDecimal price;



}
