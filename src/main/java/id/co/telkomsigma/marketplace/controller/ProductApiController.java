package id.co.telkomsigma.marketplace.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.co.telkomsigma.marketplace.dao.ProductDao;
import id.co.telkomsigma.marketplace.dao.ProductPhotoDao;
import id.co.telkomsigma.marketplace.entity.Product;
import id.co.telkomsigma.marketplace.entity.ProductPhoto;

@RestController @RequestMapping("/api/product")
public class ProductApiController {
    @Autowired private ProductDao productDao;
    @Autowired private ProductPhotoDao productPhotoDao;



    @GetMapping("/")
    public Page<Product> findProducts(Pageable page){
        return productDao.findAll(page);
    }
    
    @GetMapping("/{id}")
    public Product findById(@PathVariable("id") Product p){
        //Product p = productDao.findOne(id); // tidak perlu lagi, karena sudah disediakan Spring Data JPA
        return p;
    }
    
    @GetMapping("/{id}/photos")
    public Iterable<ProductPhoto> findPhotosForProduct(@PathVariable("id") Product p){
        return productPhotoDao.findByProduct(p);
    }



}
